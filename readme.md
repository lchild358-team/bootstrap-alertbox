# bootstrap-alertbox #

Help to display bootstrap's alert component programmatically in javascript code.

## Install ##
```bash
npm install bootstrap-alertbox
```

## Include ##
```html
<script src="node_modules/web-module/web-module.js"></script>
<script src="node_modules/bootstrap-alertbox/bootstrap-alertbox.js"></script>
```

## Usage ##
```javascript
const falertbox = module.require('bootstrap-alertbox');
const alertbox = falertbox(container);

alertbox.danger('This is error message');
```

## Methods ##

### .danger(*message*, *delay*, *timeout*)###

### .warning(*message*, *delay*, *timeout*)###

### .info(*message*, *delay*, *timeout*)###

### .success(*message*, *delay*, *timeout*)###

## Examples ##

```html
<div class="container collapse" id="alert-container"></div>

<button class="btn btn-default" id="alert-display-btn">Display</button>
```

```javascript
const falertbox = module.require('bootstrap-alertbox');
const alertbox = falertbox('#alert-container');

$('#alert-display-btn').click((e)=>{
	// Display alert message in danger theme, automatically hidden after 5s.
	alertbox.danger('This is error message', undefined, 5000);
});
```

## Bugs or feature requests
Please contact to [email](mailto:lchild358@yahoo.com.vn)

## License
[ISC](https://opensource.org/licenses/ISC)
