module.define('bootstrap-alertbox', (function(){

  const Cls = function(container){
    this.container = typeof(container) == 'object' && (container instanceof jQuery) ? container : $(container);
    this.container.empty();
    this.obj = $('<div class="alert"/>').appendTo(container);
    this.container.hide();

    this.timer = null;

    this.obj.unbind('click').bind('click', $.proxy(()=>{
      this.hide();
    }, this));
  };

  Cls.prototype._clear = function(){
    this.obj.empty();
    this.obj.removeClass('alert-danger alert-warning alert-info alert-success');
    clearTimeout(this.timer);
    return this;
  };

  Cls.prototype._show = function(message, type, interval, timeout){
    this._clear();
    this.obj.html(message);
    this.obj.addClass('alert-' + type);
    this.container.fadeIn(interval);
    if(timeout){
      this.timer = setTimeout($.proxy(()=>{
        this.hide();
      }, this), timeout);
    }
    return this;
  };

  Cls.prototype.danger = function(message, interval, timeout){
    return this._show(message, 'danger', interval, timeout);
  };

  Cls.prototype.warning = function(message, interval, timeout){
    return this._show(message, 'warning', interval, timeout);
  };

  Cls.prototype.info = function(message, interval, timeout){
    return this._show(message, 'info', interval, timeout);
  };

  Cls.prototype.success = function(message, interval, timeout){
    return this._show(message, 'success', interval, timeout);
  };

  Cls.prototype.hide = function(interval){
    clearTimeout(this.timer);
    this.container.fadeOut(interval);
    return this;
  };

  const Func = function(container){
    return new Cls(container);
  };

  return Func;

})());
